class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :name
      t.string :email
      t.text :address
      t.string :company
      t.string :phone
      t.text :location

      t.timestamps
    end
  end
end
