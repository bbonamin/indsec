class AddEstimateIdToLineItem < ActiveRecord::Migration
  def change
    add_column :line_items, :estimate_id, :integer
  end
end
