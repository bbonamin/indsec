class CartsController < ApplicationController

  def show
    if params[:id]
      @cart = Cart.find(params[:id])
    else
      @cart = Cart.find(current_cart.id)
    end
  end

  def destroy
    @cart = current_cart
    @cart.destroy
    session[:cart_id] = nil
    respond_to do |format|
      format.html { redirect_to store_url, notice: 'Your Cart is now empty.'}
    end

  end
end
