class ApplicationController < ActionController::Base
  protect_from_forgery

  # private
    def current_cart
      cart_id = session[:cart_id]
      if cart_id.nil?
        cart = Cart.create
        session[:cart_id] = cart.id
      else
        cart = Cart.find(cart_id)
      end
      cart
    end
end
