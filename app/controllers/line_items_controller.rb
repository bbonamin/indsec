class LineItemsController < ApplicationController
  respond_to :html

  def create
    @cart = current_cart
    product = Product.find(params[:product_id])

    @line_item = @cart.line_items.build(product: product)

    if @line_item.save
      redirect_to :back, notice: 'Product added to cart successfully.'
    end
  end
end
