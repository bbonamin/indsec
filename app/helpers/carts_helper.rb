module CartsHelper

  # Public: Determines whether the cart has items inside or not
  #
  # Returns a boolean
  def is_empty? cart
    cart.line_items.empty?
  end
end
