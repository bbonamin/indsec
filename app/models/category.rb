class Category < ActiveRecord::Base
  attr_accessible :description, :name

  validates :name, presence: true

  has_many :categorizations
  has_many :products, through: :categorizations
end
