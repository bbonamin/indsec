class Estimate < ActiveRecord::Base
  validates :name, :email, :address, :company, :phone, :location, presence: true
  has_many :line_items, autosave: true

  accepts_nested_attributes_for :line_items
  before_save :remove_cart_reference

  # Public: Adds the line_items  to the estimate
  #         from the cart and also removes the cart reference.
  #
  #     cart - Cart instance to migrate line_items from
  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      line_items << item 
    end
  end

  private
  # Public: TODO FIX {should be inside of add_line_items_from_cart}
  #         Removes the reference to carts before saving an estimate.
  def remove_cart_reference
    self.line_items.update_all(cart_id: nil)
  end
end
