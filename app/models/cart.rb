class Cart < ActiveRecord::Base

  has_many :line_items, dependent: :destroy
  has_many :products, through: :line_items

  accepts_nested_attributes_for :line_items

  # Public: Adds a product to the cart's line items.
  #         If there is another instance of the same product, it increments
  #         the quantity by 1, otherwise it adds a new line_item
  #
  # Returns the line item for the input product
  def add_product(product)
    current_item = line_items.find_by_product_id(product.id)

    if current_item
      current_item.quantity += 1
    else
      current_item = line_items.build(product: product, quantity: 1)
    end
    current_item
  end
end
