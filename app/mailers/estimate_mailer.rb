class EstimateMailer < ActionMailer::Base
  default from: "from@example.com"

  def estimate_request(estimate)
    @estimate = estimate
    mail to: Figaro.env.estimate_email_receiver, subject: "New estimate request"
  end
end
