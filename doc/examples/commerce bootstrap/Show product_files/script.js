jQuery(document).ready(function(){

	// Scroll to top link 
	$('a[href=#top]').click(function(){
		$('html, body').animate({scrollTop:0}, 'slow');
		return false;
	});

	$('#menu-main-menu .current-menu-item').addClass('active');
	
	
	$('#Carousel').carousel({
    	interval: 3000	
    });
	
	$('#Carousel .carousel-inner .item').first().addClass('active');
    
  }); // End jQuery()