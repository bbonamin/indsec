require 'spec_helper'

describe CartsHelper do
  describe 'is_empty?' do
    it 'returns true when the cart is empty' do
      cart = FactoryGirl.create(:cart)
      is_empty?(cart).should eq(true)
    end

    it 'returns false when the cart is empty' do
      cart = FactoryGirl.create(:cart_with_items)
      is_empty?(cart).should eq(false)
    end
  end
end
