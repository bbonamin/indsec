require 'spec_helper'

describe Estimate do
  it { should validate_presence_of :name}
  it { should validate_presence_of :email}
  it { should validate_presence_of :address}
  it { should validate_presence_of :company}
  it { should validate_presence_of :phone}
  it { should validate_presence_of :location}
  
  it { should have_many :line_items}
  


  describe '#add_line_items_from_current_cart' do
    it 'associates the estimate with the selected items' do
      cart = FactoryGirl.create(:cart_with_items)
      estimate = FactoryGirl.build(:estimate)

      line_items_count = cart.line_items.count
      estimate.add_line_items_from_cart(cart)
      
      estimate.line_items.size.should == line_items_count
      estimate.save!
      cart.line_items.reload.should be_empty
    end
  end
end
