require 'spec_helper'

describe Cart do
  it { should have_many(:line_items).dependent(:destroy)}
  it { should have_many(:products).through(:line_items)}

  describe '#add_product' do

    it 'has a quantity of one for a standard line item' do
      cart = FactoryGirl.create(:cart)
      product = FactoryGirl.create(:product)

      line_item = cart.add_product(product)

      cart.line_items.first.quantity.should == 1
    end

    it 'increases quantity by 1 when adding the same product' do
      cart = FactoryGirl.create(:cart_with_items)

      item = cart.line_items.first
      item = cart.add_product(item.product)
      item.quantity.should == 2
    end
  end
end
