require 'spec_helper'

describe Product do
	it { should validate_presence_of :name}

	let(:product) {FactoryGirl.create(:product)}
	it { should validate_uniqueness_of :name}

	it { should have_many :categorizations}
	it { should have_many(:categories).through(:categorizations)}
end
