require "spec_helper"

describe EstimateMailer do
  describe 'EstimateRequest' do
    let(:estimate) {FactoryGirl.build_stubbed(:estimate)}
    let(:mailer) {EstimateMailer.estimate_request(estimate)}


    it 'sends an email when an estimate is requested' do
      mailer.deliver
      ActionMailer::Base.deliveries.count.should eq(1)
    end

    #Ensure the receiver is correct
    it 'renders the sender email' do
      mailer.to.should == [Figaro.env.estimate_email_receiver]
    end

    #Ensure the subject is set
    it 'renders the subject' do
      mailer.subject.should == 'New estimate request'
    end

    it 'includes the requester email in the text' do
      mailer.body.encoded.should have_content(estimate.email)
    end
  end
end
