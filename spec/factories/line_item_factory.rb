FactoryGirl.define do
  factory(:line_item) do
    product
    quantity 1
    ignore do
      cart
    end
    estimate_id nil
  end
end