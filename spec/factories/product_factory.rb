FactoryGirl.define do
	factory :product do
		sequence(:name) { |n| "Product-#{n}"}
		description 'A very beautiful product'
	end
end