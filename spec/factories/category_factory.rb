FactoryGirl.define do

	factory(:category) do
		name 'TestCategory'
		description 'A Category created from the factory'
	end
end