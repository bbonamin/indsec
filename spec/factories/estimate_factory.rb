FactoryGirl.define do
  factory :estimate do
    name 'TestEstimate'
    email 'client@example.com'
    address 'Always Shining Avenue #123, San Antonio TX'
    phone '555-1234567'
    company 'NASA'
    location 'San Antonio, TX'

    factory(:estimate_with_line_items) do
      before(:create) do |estimate|
        3.times { estimate.line_items << FactoryGirl.build(:line_item, estimate: estimate)}
      end
    end
  end
end