FactoryGirl.define do
  factory :cart do
    factory :cart_with_items do
      before(:create) do |cart|
        3.times { cart.line_items << FactoryGirl.build(:line_item, cart: cart)}
      end
    end
  end
end