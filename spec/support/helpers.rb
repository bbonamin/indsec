module Helpers
	def handle_js_confirm(accept=true)
	  page.execute_script "window.original_confirm_function = window.confirm"
	  page.execute_script "window.confirmMsg = null"
	  page.execute_script "window.confirm = function(msg) { window.confirmMsg = msg; return #{!!accept}; }"
	  yield
	ensure
	  page.execute_script "window.confirm = window.original_confirm_function"
	end

  def add_items_to_cart
    products = []
    3.times { products << FactoryGirl.create(:product)}
    visit store_path
    products.each do |product|
      within "#product_#{product.id}" do
        click_button 'Add To Cart'
      end
    end
  end
end