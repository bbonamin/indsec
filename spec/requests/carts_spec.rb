require 'spec_helper'

describe 'Carts' do
  context 'when the cart has items' do
    let(:cart_with_items) { FactoryGirl.create(:cart_with_items)}

    it 'shows its items when visited' do
      visit cart_path(cart_with_items)

      cart_with_items.line_items.each do |line_item|
        page.should have_content(line_item.product.name)
      end
    end

    it 'empties the cart when using delete', js: true do
      visit cart_path(cart_with_items)

      first_product = cart_with_items.line_items.first.product

      click_button 'Empty Cart'
      handle_js_confirm do
        page.should have_content('Your Cart is now empty.')
      end
    end

    it 'instantiates a new estimate when clicking Checkout' do
      visit cart_path(cart_with_items)

      click_button 'Ask for an estimate'
      page.should have_content('Please enter your information to proceed')
    end
  end

  context 'when the cart is empty' do
    let(:cart) { FactoryGirl.create(:cart)}

    it "doesn't show the action buttons" do
      visit cart_path(cart)

      page.should_not have_button('Empty Cart')
      page.should_not have_button('Ask for a proposal')
    end

    it 'shows an explanatory message' do
      visit cart_path(cart)

      page.should have_content('Your cart is currently empty')
    end

    it 'does not have the checkout button' do
      visit cart_path(cart)
      page.should_not have_button 'Ask for an estimate'
    end
  end

end