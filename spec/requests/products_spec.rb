require 'spec_helper'

describe "Products" do
    it "creates a new product" do
      visit products_path
      click_link 'New'
      fill_in 'Name', with: 'Test product'
      fill_in 'Description', with: 'This is a very good product'
      click_button 'Save'

      page.should have_content('Product was successfully created')
    end

    it 'edits a created product' do
      product = FactoryGirl.create(:product)

      visit edit_product_path(product)
      fill_in 'Description', with: 'This is the new description'
      click_button 'Save'

      page.should have_content('Product was successfully updated')
      page.should have_content('This is the new description')
    end

    it 'destroys a product', js: true do
      product = FactoryGirl.create(:product)
      visit products_path
      click_link 'Destroy'
      handle_js_confirm do
        page.should_not have_content(product.name)
      end
    end

    context 'handling categories' do
      let(:product) { FactoryGirl.create(:product)}
      # let(:category) { FactoryGirl.create(:category)}

      it 'adds a category' do
        category = FactoryGirl.create(:category)
        visit edit_product_path(product)
        select category.name, from: 'Categories'
        click_button 'Save'
        page.should have_content('Product was successfully updated')
        page.should have_content(category.name)
      end 
    end
end
