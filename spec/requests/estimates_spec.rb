require 'spec_helper'

describe "Estimates" do
  
  it "creates a new proposal when submitting the cart" do
    add_items_to_cart
    visit current_cart_path
    click_button 'Ask for an estimate'

    fill_in 'Name', with: 'Walter White'
    fill_in 'Email', with: 'save@walterwhite.com'
    fill_in 'Address', with: 'Always alive avenue # 124, Portland, NY'
    fill_in 'Company', with: 'Sigma'
    fill_in 'Company', with: 'Sigma'
    fill_in 'Phone', with: '555-123456789'
    fill_in 'Location', with: 'USA'

    click_button 'Create Estimate'
    page.should have_content('Estimate was successfully created.')

    visit current_cart_path
    page.should have_content('Your cart is currently empty')
  end
end
