require 'spec_helper'

describe 'Store' do
  it 'displays a list of items' do
    products  = []
    3.times { products << FactoryGirl.create(:product) }
    visit store_path
    
    products.each do |p|
      page.should have_content(p.name)
      page.should have_content(p.description)
    end
  end

  it 'adds a product to the cart' do

    product = FactoryGirl.create(:product)
    visit store_path

    click_button 'Add To Cart'
    page.should have_content('Product added to cart successfully')
  end
end