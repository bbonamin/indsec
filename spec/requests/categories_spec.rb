require 'spec_helper'

describe 'Categories' do
	it "creates a new category" do
    	visit categories_path
    	click_link 'New'
    	fill_in 'Name', with: 'Test Category'
    	fill_in 'Description', with: 'Top of the line category'
    	click_button 'Save'

    	page.should have_content('Category was successfully created')
    end

    it 'edits a created category' do
    	category = FactoryGirl.create(:category)

    	visit edit_category_path(category)
    	fill_in 'Description', with: 'A new description for the category'
    	click_button 'Save'

    	page.should have_content('Category was successfully updated')
    	page.should have_content('A new description for the category')
    end

    it 'destroys a category', js: true do
    	category = FactoryGirl.create(:category)
    	visit categories_path
    	click_link 'Destroy'
    	handle_js_confirm do
    		page.should_not have_content(category.name)
    	end
    end
end